from __main__ import Site, DownloadURL
import re
import requests


class Emuparadise(Site):
    def __init__(self):
        super().__init__(r"https?://(?:www\.)?emuparadise\.me/")

    def parse(self, pageurl):
        basename = "https://www.emuparadise.me"
        # cookies = {'downloadcaptcha': '1'}
        params = {"test": "true"}
        if re.search(r"\d+-download-\d+$", pageurl):  # Dreamcast link
            gameid, isoid = re.findall(r"/(\d+)-download-(\d+)", pageurl)[0]
            params["gid"] = gameid
            params["isoid"] = isoid
        else:
            (gameid,) = re.findall(r"/(\d+)$", pageurl)
            params["gid"] = gameid
        url = (
            requests.Request("GET", f"{basename}/roms/get-download.php", params=params)
            .prepare()
            .url
        )
        return DownloadURL(basename, url, None, False)
