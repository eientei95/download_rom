from __main__ import Site, DownloadURL, get_page, get_json
from re import sub


class RomHustler(Site):
    def __init__(self):
        super().__init__(r"https?://(?:www\.)?romhustler\.(?:net|org)/")

    def parse(self, pageurl):
        """
        var dl = $(".downloadLink");
        if (dl.hasClass('split'))
            split_download = true;

        if (split_download)
            download_url = baseURL + "split_file_link/" + download_id;
        else
            download_url = baseURL + "link/" + download_id;
        """
        basename = "http://romhustler.net"
        p = get_page(pageurl)
        download_ids = []
        for i in p.findAll("a", class_="dlpage1"):
            download_ids.append(sub("d_", "", i["id"]))
        for download_id in download_ids:
            downurl = get_json(f"{basename}/link/{download_id}")["direct"]
            return DownloadURL(basename, downurl, None, False)
