from __main__ import Site, DownloadURL, get_page
from re import findall


class ArchiveOrg(Site):
    def __init__(self):
        super().__init__(r"https?://(?:www\.)?archive\.org")

    def parse(self, pageurl):
        basename = "https://archive.org"
        (ident,) = findall(r"/[^/]+/([^/]+)$", pageurl)
        base_dl = f"https://archive.org/download/{ident}/"
        p = get_page(f"{base_dl}{ident}_files.xml")
        files = [
            f["name"]
            for f in p.files.findAll("file")
            if f["source"] == "original"
            and f.format.text in ["7z", "ZIP", "RAR", "ISO Image"]
        ]
        for file in files:
            return DownloadURL(basename, f"{base_dl}{file}", None, False)
