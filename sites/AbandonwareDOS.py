from __main__ import Site, DownloadURL, get_page
from re import findall


class AbandonwareDOS(Site):
    def __init__(self):
        super().__init__(r"https?://(?:www\.)?abandonwaredos\.com/")

    def parse(self, pageurl):
        basename = "http://www.abandonwaredos.com"
        p = get_page(pageurl)
        dlc = p.findAll("div", class_="dlModalList")[0].ul.li.a["href"]
        (dlc,) = findall(r"dlc=([\dA-Za-z=]+)", dlc)
        url = f"http://www.ravestats.com/public/aw-remote-download.php?dlc={dlc}"
        return DownloadURL(basename, url, None, False)
