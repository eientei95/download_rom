from __main__ import Site, DownloadURL, get_page, post_json


class EmulatorGamesNet(Site):
    def __init__(self):
        super().__init__(r"https?://(?:www\.)?emulatorgames\.net")

    def parse(self, pageurl):
        basename = "https://www.emulatorgames.net"
        p = get_page(pageurl)
        form = p.find_all("form")[1]
        id_ = form.find("input")["value"]
        j = post_json(
            "https://www.emulatorgames.net/prompt/",
            data={"get_type": "rom", "get_id": id_},
            req_headers={"Referer": basename + form["action"]},
        )
        return DownloadURL(basename, j[0], None, False)
