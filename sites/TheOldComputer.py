from __main__ import Site, DownloadURL
from re import findall
from base64 import b64decode


class TheOldComputer(Site):
    def __init__(self):
        super().__init__(r"https?://(?:www\.)?theoldcomputer\.com/")

    def parse(self, pageurl):
        basename = "http://www.theoldcomputer.com"
        (rompath,) = findall(r"file=([^&]+)", pageurl)
        rompath = b64decode(rompath).decode("utf-8")
        if rompath.startswith("/"):
            rompath = rompath[1:]
        url = f"{basename}/roms/{rompath}"
        return DownloadURL(basename, url, None, False)
