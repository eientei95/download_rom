from __main__ import Site, DownloadURL, get_page, post_page, parse_form
from requests import Request


class DownloadROMsIO(Site):
    def __init__(self):
        super().__init__(r"https?://(?:www\.)?downloadroms\.io")

    def parse(self, pageurl):
        basename = "https://www.downloadroms.io"
        p = get_page(pageurl)
        f = p.find("form", {"id": "download-form"})
        url, params = parse_form(f, basename)
        p = post_page(url, data={"mediaId": f.button["dlid"]})
        f = p.find("form", {"name": "redirected"})
        url, params = parse_form(f, basename)
        r = Request("GET", url, params=params).prepare()
        return DownloadURL(basename, r.url, None, False)
