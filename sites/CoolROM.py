from __main__ import Site, DownloadURL, get_page
from re import findall


class CoolROM(Site):
    def __init__(self):
        super().__init__(r"https?://(?:www\.)?coolrom\.com(?:\.au)?")

    def parse(self, pageurl):
        basename = "https://coolrom.com.au"
        (id_,) = findall(r"/roms/[^/]*/(\d+)/", pageurl)
        p = get_page(f"https://coolrom.com.au/dlpop.php?id={id_}")
        url = (
            findall(
                r'action="([^"]+)',
                p.find("script", attrs={"language": "javascript"}).text,
            )[0]
            + "?"
        )
        print(url)
        return DownloadURL(basename, url, None, False)
