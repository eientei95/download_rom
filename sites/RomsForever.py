from __main__ import Site, DownloadURL, get_page
from re import sub


class RomsForever(Site):
    def __init__(self):
        super().__init__(r"https?://(?:www\.)?romsforever\.co")

    def parse(self, pageurl):
        basename = "https://romsforever.co"
        if "?download" not in pageurl:
            p = get_page(pageurl + "?download=0")
        else:
            p = get_page(sub(r"download=\d+", r"download=0", pageurl))
        d = p.find("div", {"id": "download-active"}).a["href"]
        return DownloadURL(basename, d, None, False)
