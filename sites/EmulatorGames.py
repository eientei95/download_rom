from __main__ import Site, DownloadURL, get_page, post_json
from re import findall


class EmulatorGames(Site):
    def __init__(self):
        super().__init__(r"https?://(?:www\.)?emulator\.games")

    def parse(self, pageurl):
        basename = "https://emulator.games"
        p = get_page(pageurl)
        p = get_page(p.find("a", {"id": "rom-link"})["href"])
        (set_id,) = findall(r"var set_id = (\d+)", str(p))
        (set_type,) = findall(r'var set_type = "([^"]+)', str(p))
        j = post_json(
            "https://emulator.games/get.php",
            data={"set_id": set_id, "set_type": set_type},
        )
        return DownloadURL(
            basename, j[3], None, True
        )  # Redirects you to the emulatorgames.net page
